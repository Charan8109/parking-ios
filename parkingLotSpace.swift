//
//  parkingLotSpace.swift
//  ParkingSystemiOS
//
//  Created by BALAJI ABHISHEK on 2/5/17.
//  Copyright © 2017 sivaramsomanchi. All rights reserved.


import UIKit
import Parse
import Bolts

class parkingLotSpace:PFObject,PFSubclassing{
    @NSManaged var lotNumber:Int
    @NSManaged var parkingSpace:Int
    @NSManaged var category:Int

    static func parseClassName() -> String {
        return "parkingLotSpace"
    }
}
