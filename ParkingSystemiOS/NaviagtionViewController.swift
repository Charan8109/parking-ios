//
//  NaviagtionViewController.swift
//  ParkingSystemiOS
//
//  Created by admin on 4/6/17.
//  Copyright © 2017 sivaramsomanchi. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


class NaviagtionViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var locationPickerPV: UIPickerView!
    @IBOutlet weak var mapView: MKMapView!
    var startingLocation: CLLocationCoordinate2D! = CLLocationCoordinate2D(latitude: 40.35082, longitude: -94.88245540000003)
    var destinationLocation: CLLocationCoordinate2D!
    let locationManager = CLLocationManager()
    var locations: [String] = ["Hudson Hall", "Valk Building", "Administrative Building", "Forest Village Apartments", "Bearcat Stadium"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        mapView.delegate = self
        mapView.mapType = MKMapType.Standard
        mapView.showsUserLocation = true
        self.locationManager.startUpdatingLocation()
        // Do any additional setup after loading the view.
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        startingLocation = location?.coordinate
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // The number of columns of data
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return locations.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return locations[row]
    }
    
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if row == 0{
            destinationLocation = CLLocationCoordinate2D(latitude: 40.350551, longitude: -94.880273)
        }
        else if row == 1{
            destinationLocation = CLLocationCoordinate2D(latitude: 40.3518491, longitude: -94.87972609999997)
        }
        else if row == 2{
            destinationLocation = CLLocationCoordinate2D(latitude: 40.35372479999999, longitude: -94.88285409999997)
        }
        else if row == 3{
            destinationLocation = CLLocationCoordinate2D(latitude: 40.3564334, longitude: -94.88600450000001)
        }
        else if row == 4{
            destinationLocation = CLLocationCoordinate2D(latitude: 40.3506406, longitude: -94.88584759999998)
        }
        drawLines(destinationLocation.latitude, longitude: destinationLocation.longitude)

        
    }
    
    
    
    //This function helps in drawing the lines across the mapview with respect to latitude and longitude.
    func drawLines(latitude:Double, longitude:Double){
      
        
        if startingLocation != nil && destinationLocation != nil{
            
            let sourceLocation:CLLocationCoordinate2D! = startingLocation
            let destinationLocations:CLLocationCoordinate2D! = destinationLocation
            // 3.
            let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
            let destinationPlacemark = MKPlacemark(coordinate: destinationLocations, addressDictionary: nil)
            
            // 4.
            let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
            let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
            
            // 5.
            let sourceAnnotation = MKPointAnnotation()
            
            if let location = sourcePlacemark.location {
                sourceAnnotation.coordinate = location.coordinate
            }
            
            
            let destinationAnnotation = MKPointAnnotation()
            
            if let location = destinationPlacemark.location {
                destinationAnnotation.coordinate = location.coordinate
            }
            
            // 6.
            self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
            
            // 7.
            let directionRequest = MKDirectionsRequest()
            directionRequest.source = sourceMapItem
            directionRequest.destination = destinationMapItem
            directionRequest.transportType = .Automobile
            
            // Calculate the direction
            let directions = MKDirections(request: directionRequest)
            print(directionRequest)
            // 8.
            directions.calculateDirectionsWithCompletionHandler {
                (response, error) -> Void in
                
                guard let response = response else {
                    if let error = error {
                        print("Error: \(error)")
                    }
                    
                    return
                }
                
                let route = response.routes[0]
                print(response.routes)
                self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.AboveRoads)
                let rect = route.polyline.boundingMapRect
                self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
            }
            
        }
    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = UIColor.redColor()
            renderer.lineWidth = 4.0
            
            return renderer
        
        
    }
    
    
}
