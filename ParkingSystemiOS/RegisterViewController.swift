//
//  RegisterViewController.swift
//  ParkingSystemiOS
//
//  Created by Vaddadi,Saicharan on 11/13/16.
//  Copyright © 2016 sivaramsomanchi. All rights reserved.
//

import UIKit
import Parse
import Bolts

class RegisterViewController: UIViewController {

        
        @IBOutlet weak var emailTF: UITextField!
        @IBOutlet weak var passwordTF: UITextField!
//        @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var confirmpswdTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func displayMyAlert(userMessage:String)
    {
        let myAlert = UIAlertController(title:"Alert", message:userMessage, preferredStyle:UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "OK", style:UIAlertActionStyle.Default, handler: nil)
        myAlert.addAction(okAction)
        self.presentViewController(myAlert, animated: true, completion: nil)
    }
    
    
    
        @IBAction func register(sender: AnyObject) {
            // Defining the user object
            let userName = emailTF.text
            let userPassword = passwordTF.text
            let userConfirmPassword = confirmpswdTF.text
            
            
            
            if(userName!.isEmpty||userPassword!.isEmpty||userConfirmPassword!.isEmpty)
            {
                
                
                self.displayMyAlert("All fields are required to fill in")
                
                return
            }
            if(userPassword != userConfirmPassword)
            {
                
                
                self.displayMyAlert("Passwords do not match")
                
                return
            }
            // Store Data
            NSUserDefaults.standardUserDefaults().setObject(userName, forKey: "userEmail")
            NSUserDefaults.standardUserDefaults().setObject(userPassword, forKey: "userPassword")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            //Profile Picture setup
            
//            let profileImageData = UIImageJPEGRepresentation(profilePicImageView.image!, 1)
            let myUser:PFUser = PFUser()
            myUser.username = userName
            myUser.password = userPassword
            myUser.email = userName
//            myUser.setObject(userFirstName!, forKey: "first_name")
//            myUser.setObject(userLastName!, forKey: "last_name")
            
//            if(profileImageData != nil)
//            {
//                let profileImageFile = PFFile(data: profileImageData!)
//                myUser.setObject(profileImageFile!, forKey: "profilePic")
//            }
            
            myUser.signUpInBackgroundWithBlock{ (success:Bool, error:NSError?) -> Void in
                var userMessage = "Registration is successful.. Please Check your email and click on the verification link!"
                if(!success)
                {
                    userMessage = error!.localizedDescription
                }
                
                let myAlert = UIAlertController(title:"Alert", message:userMessage, preferredStyle:UIAlertControllerStyle.Alert)
                let okAction = UIAlertAction(title: "OK", style:UIAlertActionStyle.Default){ action in
                    if(success)
                    {
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                }
                
                myAlert.addAction(okAction)
                self.presentViewController(myAlert, animated: true, completion: nil)
                
            }
            
            
            
    }
    
    
        }
        

        
        
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
         // Get the new view controller using segue.destinationViewController.
         // Pass the selected object to the new view controller.
         }
         */
        
