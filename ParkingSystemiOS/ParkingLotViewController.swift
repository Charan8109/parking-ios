//
//  ParkingLotViewController.swift
//  ParkingSystemiOS
//
//  Created by Student on 12/4/16.
//  Copyright © 2016 sivaramsomanchi. All rights reserved.
//

import UIKit

class ParkingLotViewController: UIViewController {
    var appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var lot63TF: UILabel!
    
    @IBOutlet weak var lot62TF: UILabel!
  
    @IBOutlet weak var lot60TF: UILabel!
    @IBOutlet weak var lot21TF: UILabel!
    @IBOutlet weak var lot12TF: UILabel!
    
    @IBOutlet weak var lot42TF: UILabel!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(("enter text"))
        lot63TF.text!=String(appdelegate.a)
        lot62TF.text!=String(appdelegate.b)
        lot60TF.text!=String(appdelegate.c)
        lot21TF.text!=String(appdelegate.d)
        lot12TF.text!=String(appdelegate.e)
        lot42TF.text!=String(appdelegate.f)
        print("text entered")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func RLot63RBTN(sender: UIButton) {
        appdelegate.a = appdelegate.a - 1
        lot63TF.text=String(appdelegate.a)
        //displayMessage("Your Parking Reserved")
        
    }

    @IBAction func VLot63VBTN(sender: AnyObject) {
        appdelegate.a = appdelegate.a + 1
        lot63TF.text=(String)(appdelegate.a)
        displayMessage("You can vacate")
    }
    
    
    @IBAction func RLot62BTN(sender: AnyObject) {
        appdelegate.b = appdelegate.b - 1
        lot62TF.text=(String)(appdelegate.b)
        displayMessage("Your Parking Reserved")
        
    }
    
    
    @IBAction func VLot62BTN(sender: AnyObject) {
        appdelegate.b = appdelegate.b + 1
        lot62TF.text=(String)(appdelegate.b)
        displayMessage("You can vacate")
        
    }
    
    
    
    @IBAction func RLot60BTN(sender: AnyObject) {
        lot60TF.text=(String)(Int(lot60TF.text!)!-1)
        displayMessage("Your Parking Reserved")
        

    }
    
    
    @IBAction func VLot60BTN(sender: AnyObject) {
        
        lot60TF.text=(String)(Int(lot60TF.text!)!+1)
        displayMessage("You can vacate")
    }
    
    
    
    
    @IBAction func RLot21BTN(sender: AnyObject) {
        lot21TF.text=(String)(Int(lot21TF.text!)!-1)
        displayMessage("Your Parking Reserved")
        

    }
    
    
    
    @IBAction func VLot21BTN(sender: AnyObject) {
        lot21TF.text=(String)(Int(lot21TF.text!)!+1)
        displayMessage("You can vacate")
    }
    
    
    
    @IBAction func RLot12BTN(sender: AnyObject) {
        lot12TF.text=(String)(Int(lot12TF.text!)!-1)
        displayMessage("Your Parking Reserved")
        

    }
    
    
    @IBAction func VLot12BTN(sender: AnyObject) {
        lot12TF.text=(String)(Int(lot12TF.text!)!+1)
        displayMessage("You can vacate")
    }
    
    
    
    @IBAction func RLot42BTN(sender: AnyObject) {
        lot42TF.text=(String)(Int(lot42TF.text!)!-1)
        displayMessage("Your Parking Reserved")
        

    }
    
    
    @IBAction func VLot42BTN(sender: AnyObject) {
        lot42TF.text=(String)(Int(lot42TF.text!)!+1)
       // displayMessage("Your can vacate")
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 */
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
 
 }
}
