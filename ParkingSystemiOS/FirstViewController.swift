//
//  FirstViewController.swift
//  ParkingSystemiOS
//
//  Created by BALAJI ABHISHEK on 10/21/16.
//  Copyright © 2016 sivaramsomanchi. All rights reserved.
//

import UIKit
import Parse
import Bolts

class LoginViewController: UIViewController {
    
    @IBAction func newUserBTN(sender: AnyObject) {
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle:nil).instantiateViewControllerWithIdentifier("SignUp")
        self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var usernameTF: UITextField!
    //   @IBAction func login(sender: UIButton) {
    //        let username = self.userNameTF.text!
    //        let password = self.passwordTF.text!
    
    //        PFUser.logInWithUsernameInBackground(String(usernameTF.text!), password: passwordTF.text!, block: { (user, error) -> Void in
    //
    //            if ((user) != nil) {
    //                let alert = UIAlertController(title: "Success", message:"Logged In!", preferredStyle: .Alert)
    //                alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
    //                //                    self.presentViewController(alert, animated: true){}
    //
    //                dispatch_async(dispatch_get_main_queue(), { () -> Void in
    //                    let viewController:UIViewController = UIStoryboard(name: "Main", bundle:nil).instantiateViewControllerWithIdentifier("HomePageTB")
    //                    self.presentViewController(viewController, animated: true, completion: nil)
    //                })
    //            } else {
    //                let alert = UIAlertController(title: "Error!", message:"Username and password do not match. Please check your credentials.", preferredStyle: .Alert)
    //                alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
    //                self.presentViewController(alert, animated: true){}
    //            }
    //        })
    //
    //    }
    @IBAction func login(sender: AnyObject) {
        PFUser.logInWithUsernameInBackground(usernameTF.text!, password: passwordTF.text!,
                                             block:{(user, error) -> Void in
                                                if error != nil{
                                                    //print(error)
                                                    self.displayMessage("Incorrect Password or Username")
                                                }
                                                else {
                                                    // Everything went alright here
                                                    
                                                    let my:UIStoryboard =  UIStoryboard(name: "Main", bundle: nil)
                                                    
                                                    let right:pickerViewController =
                                                        my.instantiateViewControllerWithIdentifier("htab") as! pickerViewController
                                                    self.showViewController(right, sender: "sai")
                                                    
                                                    
                                                    
                                                }
        })
    }
    
    //    @IBAction func signup(sender: UIButton) {
    //    }
    //
    //    @IBAction func forgotpassword(sender: UIButton) {
    //    }
    //
    //    @IBAction func emergency(sender: UIButton) {
    //    }
    //
    //
    //
    
    
    
    
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func displayAlertWithTitle(title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    
    
    
    
}

