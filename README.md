This repository for?

To create an iOS application with the requirements as follows. The basic idea is that the police station on campus wants to build a parking lot vacancy system that tracks the status of each parking lot in Northwest. Whenever a driver comes or goes, he/she updates the vacancy status via his/her smartphone. As a result, it is convenient for people to find an empty parking spot.

Team members:

Siva Sai Rama Krishna Somanchi,
Sai Charan Vaddadi,
Abhishek Kavalla,
Santhosh Kumar Thadka